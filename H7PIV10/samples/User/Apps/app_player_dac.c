#include "app_config.h"

#ifdef app_player_dac

osThreadId app_handler_player_dac;

/*********************************************************************


**********************************************************************/
void app_player_dac_task(void const * argument)
{
	while(1)
	{
	}
}

/*********************************************************************


**********************************************************************/
void app_player_dac_start(void)
{	

  osThreadDef(player_dac_task, app_player_dac_task, osPriorityHigh, 1, (1024*100));
	app_handler_player_dac = osThreadCreate(osThread(player_dac_task), NULL);
}

#endif