#include "recorder_adc.h"
#include "string.h"

recorder_adc_dev_t recorder_adc_dev;
/******************************************************
 * 
 * adc data struct init
 * 
 * ***************************************************/
void recorder_adc_data_init(void)
{

}
/*****************************************************
*
*
*   recorder adc init
*
*****************************************************/
void recorder_adc_adc_init(void)
{
	
}
/******************************************************
 * 
 * recorder adc timer init(void)
 * 
 * ***************************************************/
void recorder_adc_timer_init(void)
{
	
}
/*****************************************************
 * 
 * recorder deinit
 * 
 * **************************************************/
void recorder_adc_deinit(void)
{
    recorder_adc_data_init();
    recorder_adc_timer_init();
}

/****************************************************
 * 
 * recorder get status
 * 
 * **************************************************/
recorder_dev_status_t recorder_adc_get_status(void)
{
	return recorder_adc_dev.status;
}
/*****************************************************
 * 
 * 
 * set recorder sample rate
 * 
 * 
 * ***************************************************/
uint8_t recorder_adc_set_samplerate(uint32_t rate)
{
	uint32_t counter;
	uint32_t freq;
	
	
	return 1;
}
/*****************************************************
 * 
 * set recorder buffer status
 * @param buffer
 * buffer address pointer
 * @return
 * 0:    invalid
 * 1:    valid
 * 0xff: err
 * 
 * ***************************************************/
uint8_t recorder_adc_set_buffer_valid(uint16_t* buffer, uint8_t state)
{
	if(buffer == recorder_adc_dev.buffer0)
	{
		recorder_adc_dev.buffer_valid[0] = state;
		return recorder_adc_dev.buffer_valid[0];
	}
	else if(buffer == recorder_adc_dev.buffer1)
	{
		recorder_adc_dev.buffer_valid[1] = state;
		return recorder_adc_dev.buffer_valid[1];
	}
	else
	{
		return 0xff;
	}
}
/*****************************************************
 * 
 * 
 * recoreder stop
 * 
 * 
 * ***************************************************/
uint8_t recorder_adc_stop(void)
{
	uint8_t res = 0;
	//
	//HAL_OK(value: 0) will be return by HAL api if run successfully
	res += HAL_TIM_Base_Stop(RECORDER_TIMER_HANDLE);
	res += HAL_ADC_Stop_DMA(RECORDER_ADC_HANDLER); 
//	if( res <= 1)
//	{
	recorder_adc_data_init();
	recorder_adc_dev.status = RECORDER_STOP;
//	}
	return res;
}
/*****************************************************
 * 
 * recorder start
 * 
 * ***************************************************/
uint8_t recorder_adc_start(void)
{
	uint8_t res = 0;
	recorder_adc_dev.status = RECORDER_START;
	//
	//HAL_OK(value: 0) will be return by HAL api if run successfully
	if(recorder_adc_dev.buffer_valid[recorder_adc_dev.buffer_index])
	{
		res += HAL_TIM_Base_Start(RECORDER_TIMER_HANDLE);
		
		res += HAL_ADC_Start_DMA(RECORDER_ADC_HANDLER, (uint32_t*)(recorder_adc_dev.buffer_switch[recorder_adc_dev.buffer_index]), RECORDER_BUFFER_SIZE*2);
		if (res == 0)
		{
			recorder_adc_dev.status = RECORDER_RECORDING;
		}
	}
	return !res;
}


/*****************************************************
 * 
 * player get buffer address
 * return the address of buffer
 * 
 * ***************************************************/
uint16_t* recorder_adc_get_buffer_pos(void)
{
	if(recorder_adc_dev.status == RECORDER_STOP && recorder_adc_dev.buffer_valid[0]==0)
	{
		return recorder_adc_dev.buffer0;
	}
	else if(recorder_adc_dev.buffer_index == 0 && recorder_adc_dev.status == RECORDER_RECORDING && recorder_adc_dev.buffer_valid[1]==0)
	{
		return recorder_adc_dev.buffer1;
	}
	else if(recorder_adc_dev.buffer_index == 1 && recorder_adc_dev.status == RECORDER_RECORDING && recorder_adc_dev.buffer_valid[0]==0)
	{
		return recorder_adc_dev.buffer0;
	}
	return NULL;
}



/*****************************************************
 * 
 * 
 * stm32 irq callback,
 * 
 * 
 * **************************************************/
void recorder_adc_irq_conv_complete(void)
{
	HAL_TIM_Base_Stop(RECORDER_TIMER_HANDLE);
	recorder_adc_dev.status = RECORDER_PAUSE;
	//
	//clear buffer
	memset(recorder_adc_dev.buffer_switch[recorder_adc_dev.buffer_index],0,RECORDER_BUFFER_SIZE);
	recorder_adc_dev.buffer_valid[recorder_adc_dev.buffer_index]=0;
	//
	//set to play the other buffer if valid.
	if(recorder_adc_dev.buffer_valid[(!recorder_adc_dev.buffer_index)]==1)
	{
		recorder_adc_dev.buffer_index = !recorder_adc_dev.buffer_index;
		recorder_adc_start();
	}
	else
	{
		recorder_adc_stop();
	}
}

/*****************************************************
 * 
 * 
 * device init
 * 
 * 
 * **************************************************/
void recorder_dev_adc_init(recorder_dev_t* dev)
{
	dev->delay_ms                = RECORDER_DELAY_MS;
	dev->start            = recorder_adc_start;
	dev->stop             = recorder_adc_stop;
	dev->set_samplerate   = recorder_adc_set_samplerate;
	dev->set_buffer_valid = recorder_adc_set_buffer_valid;
	dev->get_status       = recorder_adc_get_status;
	dev->get_buffer_pos   = recorder_adc_get_buffer_pos;
	dev->buffer_size      = RECORDER_BUFFER_SIZE;
	dev->deinit           = recorder_adc_deinit;
	
	recorder_adc_adc_init();
	recorder_adc_data_init();
	recorder_adc_timer_init();
}

